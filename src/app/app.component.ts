import { Component, OnInit } from '@angular/core';
import {GeneralService} from './services/general/general.service';

@Component({
  selector: 'app-root',
  template: `
  <router-outlet></router-outlet>
  <div class="loading-spinner" *ngIf="loading">
    <div>
      <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
      </div>
      <p class="text-center text-white">Loading</p>
    </div>
  </div> 
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  loading;

  constructor(
    private general:GeneralService,
  ){  }

  ngOnInit(): void {
    this.general.cast.subscribe(ld=> this.loading = ld);
  }
  
}
