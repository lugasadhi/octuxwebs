import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { auth } from 'firebase';
import { Router } from '@angular/router';
import {GeneralService} from '../general/general.service';
import { Observable } from 'rxjs/internal/Observable';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    public angularFireAuth: AngularFireAuth,
    public router: Router,
    public general:GeneralService,
    public firestore:AngularFirestore,
    ) { 
    }

    allowRole = ['master', 'accountServicing', 'payrollBilling']

    async login(email: string, password: string) {
      return await this.angularFireAuth.auth.signInWithEmailAndPassword(email, password);
    }
    async register(email: string, password: string) {
      return await this.angularFireAuth.auth.createUserWithEmailAndPassword(email, password)
    }
  
    async sendEmailVerification() {
      return await this.angularFireAuth.auth.currentUser.sendEmailVerification();
    }
  
    async sendPasswordResetEmail(passwordResetEmail: string) {
      return await this.angularFireAuth.auth.sendPasswordResetEmail(passwordResetEmail);
    }

    async logout() {
      this.general.deleteCookie("token");
      this.general.deleteCookie("rememberme");
      this.general.deleteLocalStorage("userdata");
      return await this.angularFireAuth.auth.signOut();
    }

    async resetPassword(actionCode, newpass){
      return await this.angularFireAuth.auth.confirmPasswordReset(actionCode,newpass);
    }
  
    async  loginWithGoogle() {
      return await this.angularFireAuth.auth.signInWithPopup(new auth.GoogleAuthProvider())
    }

    getLoggedInUser(){
      return this.angularFireAuth.authState;
    }
    
    checkRole(role){
      return role != undefined? this.allowRole.indexOf(role) >= 0: false;
    }

    getToken(){      
      if(this.general.getCookies('token') != undefined){
        return this.general.getCookies("token");
      }else{
        return this.general.getLocalStorage("token");
      }
    }

    ifTokenValid(){
      console.log(this.angularFireAuth.idToken);
    }

    async getUserDt(user){
      return this.angularFireAuth.auth.onAuthStateChanged(user);
    }

    getUserData(){
    let data = new Observable(x => {
      
    });

    return data;

    // https://octux.ga/account/admin

  }


}


