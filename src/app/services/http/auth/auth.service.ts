import { Injectable } from '@angular/core';
import {AccessService} from '../access.service';
import {GeneralService} from '../../general/general.service';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private access:AccessService,
    private http:HttpClient,
    private general:GeneralService,
    private route:Router,
    ) { }

   login(email, password){
    let hdr = {
      headers : this.access.getContentTypeHeader(),
    };
    let param = {
      email:email,
      password:password
    };
    return  this.http.post<login>(this.access.host()+"/login/email",param,hdr);
  }

  logout() {
    this.general.setLoading(false);
    this.general.deleteCookie("token");
    this.general.deleteCookie("rememberme");
    this.general.deleteLocalStorage("userdata");
    this.route.navigate(['/auth/login']);
  }

  forgetPassword(email){
    let param = {email:email}
    return this.http.get(this.access.host()+"/reset/email/admin",  { params: param } );
  }

  resetPassword(newpass, confirmpass, token){
    let param = {
      password:newpass,
      confirmPassword:confirmpass, 
      resetPasswordToken:token
    }
    return this.http.post(this.access.host()+"/reset/email/admin",  param );
  }

  getUserData(){
    let hdr = {
      headers : this.access.getHeader(),
    }
    return this.http.get(this.access.host()+"/account/admin",  hdr );
  }

  getUserDataByParam(adminId, token){
     let param = {
      adminId: adminId,
      nextPageToken: token
    }
    let hdr = {
      headers : this.access.getHeader(),
      params: param 
    }
    return this.http.get(this.access.host()+"/account/admin",  hdr );
  }

}



// interface
interface login{
  adminAuthToken:string
}