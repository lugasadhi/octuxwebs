import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import {GeneralService} from '../general/general.service';

@Injectable({
  providedIn: 'root'
})
export class AccessService {

  constructor(
    private general:GeneralService
  ) { }

  host(){
    return 'https://octux.ga';
  }

  getHeader(){
    let token = this.general.getToken();
    let headAuth = new HttpHeaders({
      'X-AUTH-TOKEN': token,
      'Content-Type':'application/json'
    })    

    return headAuth;
  }

  getContentTypeHeader(){
    let headAuth = new HttpHeaders({
      'Content-Type':'application/json'
    })    

    return headAuth;
  }

}
