import { Injectable } from '@angular/core';
import {AccessService } from '../access.service';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ClaimService {

  constructor(
    private _access:AccessService,
    private http:HttpClient,
  ) { }

  api="/claim";

  get(parameter){
    let hdr = {
      headers : this._access.getHeader(),
      params: parameter 
    }
    return this.http.get(this._access.host()+this.api, hdr);
  }



}
