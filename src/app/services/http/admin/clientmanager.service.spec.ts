import { TestBed } from '@angular/core/testing';

import { ClientmanagerService } from './clientmanager.service';

describe('ClientmanagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ClientmanagerService = TestBed.get(ClientmanagerService);
    expect(service).toBeTruthy();
  });
});
