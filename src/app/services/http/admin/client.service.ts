import { Injectable } from '@angular/core';
import {AccessService } from '../access.service';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(
    private _access:AccessService,
    private http:HttpClient,
  ) { }

  get<clientIntf>(id, page){    
    let param = {
      clientId: id,
      pageNumber: page
    }
    let hdr = {
      headers : this._access.getHeader(),
      params: param 
    }
    return this.http.get(this._access.host()+"/client", hdr);
  }

  post(param){
    let hdr = {
      headers : this._access.getHeader(),
    }
    return this.http.post(this._access.host()+"/client",param,hdr);
  }

  put(param){
    let hdr = {
      headers : this._access.getHeader(),
    }
    return this.http.put(this._access.host()+"/client",param,hdr);
  }

  delete(id){
    let param = {
      clientId: id
    }
    let hdr = {
      headers : this._access.getHeader(),
      body: param 
    }
    return this.http.request('delete',this._access.host()+"/client", hdr);
  }

  getDraft(id,page){
    let param = {
      draftId: id,
      pageNumber: page
    }
    let hdr = {
      headers : this._access.getHeader(),
      params: param 
    }
    return this.http.get(this._access.host()+"/draft/client", hdr);
  }

  postDraft(param){
    let hdr = {
      headers : this._access.getHeader(),
    }
    return this.http.post(this._access.host()+"/draft/client",param,hdr);
  }

  putDraft(param){
    let hdr = {
      headers : this._access.getHeader(),
    }
    return this.http.put(this._access.host()+"/draft/client",param,hdr);
  }

  deleteDraft(id){
    let param = {
      draftId: id
    }
    let hdr = {
      headers : this._access.getHeader(),
      body: param 
    }
    return this.http.request('delete',this._access.host()+"/draft/client", hdr);
  }
  
}


interface clientIntf{
  arrangement:string,
  clientId:string,
  clientName:string,
  createdById:string,
  createdByName:string,
  docType:string,
  officeAddressLine1:string,
  officeAddressLine2:string,
  templates: Array<object>,
  wowId:string,
}