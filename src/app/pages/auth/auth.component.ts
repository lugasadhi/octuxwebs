import { Component, OnInit, OnDestroy } from '@angular/core';
import * as $ from 'jquery';


@Component({
  selector: 'ng-shared-auth',
  template: `
    <div class="bg-login" > 
      <div class="bg-filter">
        <auth-header></auth-header>
        <div class="login full-height">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `,
  styleUrls: [
    './general.scss'
  ]
})
export class AuthComponent  implements OnInit{

  constructor(

  ) {
  }

    
    ngOnInit() {
      $('body').addClass('login-bg');
    }

    ngOnDestroy(){
      $('body').removeClass('login-bg');
    }
 
}
