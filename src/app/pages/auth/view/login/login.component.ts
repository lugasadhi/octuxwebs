import { Component, OnInit } from '@angular/core';
import {FormControl, Validators, FormGroup} from '@angular/forms';

import {GeneralService} from '../../../../services/general/general.service';
import { Router } from '@angular/router';

import {AuthService} from '../../../../services/http/auth/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: [
    './login.component.scss',
    '../../general.scss'
  ]
})

export class LoginComponent implements OnInit {
  formActive = true;
  usernameNotValid = false;
  passwordNotValid = false;
  value = '';
  loginForm : FormGroup;
  showPass = false;


  constructor(
    private authService: AuthService,
    private router:Router,
    private general:GeneralService, 
  ) { }
  
 
  
  ngOnInit() {
    this.loginForm = new FormGroup({
      username : new FormControl('', [
        Validators.required,
        Validators.email,
      ]),
      password : new FormControl('', [
        Validators.required,
      ]),
      rememberme: new FormControl(false,[])
    });
  }

  deleteUserInvalidErr(event: any){
    this.usernameNotValid = false;
  }

  deletePassInvalidErr(event: any){
    this.passwordNotValid = false;
  }

  usernotvalidmsg;
  onSubmit(){
    if(this.loginForm.valid){
      this.general.setLoading(true);
      this.authService.login(this.loginForm.value.username, this.loginForm.value.password).subscribe((res) => {
          
        this.general.setLoading(false);
        console.log(res.adminAuthToken );
        
          if(res.adminAuthToken != undefined){
            if(this.loginForm.value.rememberme){
              this.general.setCookies("token",res.adminAuthToken,100*24);
              this.general.setCookies("rememberme",true,100*24);
            }else{
              this.general.setCookies("token",res.adminAuthToken,24);
              this.general.setSessionStorage("token",res.adminAuthToken);
            }
            this.router.navigate(['/admin']);
          }else{
            this.usernameNotValid = true;
            this.usernotvalidmsg="Invalid email";
          }

        },(err)=>{
          console.log(err);
          
          this.general.setLoading(false);
          if(err.error.message == "Email address not found!"){
            this.usernameNotValid = true;
            this.usernotvalidmsg="Invalid email";
          }else if(err.error.message == "Invalid password for Admin!"){
            this.passwordNotValid = true;
          }
          console.log(err.error);
        }
      )
    }
  }


}


