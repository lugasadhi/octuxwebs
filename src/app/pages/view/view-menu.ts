export const MENU_ITEMS = [
  {
    title: 'Request',
    group: true,
  },
  {
    title: 'Request',
    icon: 'nb-compose',
    children: [
      {
        title: 'New Request',
        link: '/pages/request/new',
      },
      {
        title: 'List Request',
        link: '/pages/request/list',
      },
    ],
  }
];
