import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ViewComponent } from './view.component';
import { HomeComponent } from './home/home.component';



const routes: Routes = [{
  path: '',
  component: ViewComponent,
  children: [{
    path: 'home',
    component: HomeComponent,
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full',
  }, 
    // {
    //   path: '**',
    //   component: NotFoundComponent,
    // }
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  // providers:[AuthGuard]
})
export class ViewRoutingModule {
}
