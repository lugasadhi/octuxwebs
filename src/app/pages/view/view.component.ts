import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MENU_ITEMS } from './view-menu';


@Component({
  selector: 'ng-shared-view',
  template: `
    <div>
      <app-menu></app-menu>
      <router-outlet></router-outlet>
    </div>
    <app-footer></app-footer>
  `,
})
export class ViewComponent  implements OnInit{

  menu = MENU_ITEMS;

  constructor() {
   }

   ngOnInit(){
   }
 
}
