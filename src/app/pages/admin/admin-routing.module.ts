import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminComponent } from './admin.component';

import { DashboardComponent } from './view/home/dashboard.component';
import { BlockchainComponent } from './view/blockchain/blockchain.component';



const routes: Routes = [
 
  {
    path: '',
    component: AdminComponent,
    children: [{
        path: 'home',
        component: DashboardComponent,
      },
      { path: 'task', loadChildren: './view/task/task.module#TaskModule'},
      { path: 'dashboard', loadChildren: './view/dashboard/dashboard.module#DashboardModule'},
      {
        path: 'blockchain-console',
        component: BlockchainComponent,
      },

      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      }, 
      
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  // providers:[AuthGuard]
})
export class AdminRoutingModule {
}
