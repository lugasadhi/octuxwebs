import { Component, OnInit, OnDestroy } from '@angular/core';
import {GeneralService} from '../../../../services/general/general.service';
import {AuthService} from '../../../../services/http/auth/auth.service';


@Component({
    selector: 'admin-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
  }
)

export class HeaderComponent implements OnInit {


  user;

  constructor(
    private auth:AuthService,
    private general:GeneralService
  ) { }


  ngOnInit() {

    this.auth.getUserData().subscribe((resp)=>{
      this.user=resp;
    },(err)=>{
      console.log(err);
      if(err.error.message == "Invalid authToken provided."){
        this.general.setLoading("false");
        this.auth.logout();
      }
    });
    
  }

  logout(){
    this.auth.logout();
  }


}

