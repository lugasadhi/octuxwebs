export const menu=[
  {
    title:"home",
    path:"admin/home",
    icon:"assets/images/icon/dashboard.png",
  },
  {
    title:"dashboard",
    icon:"assets/images/icon/activity.png",
    child:[
      {
        title:"my associates",
        path:"admin/dashboard/accosiates",
      },
      {
        title:"claims",
        path:"admin/dashboard/claims",
      },
      {
        title:"leave",
        path:"admin/dashboard/leave",
      },
      {
        title:"timesheet",
        path:"admin/dashboard/timesheet",
      },
      {
        title:"payslip",
        path:"admin/dashboard/payslip",
      },
      {
        title:"reports",
        path:"admin/dashboard/reports",
      }
    ]
  },
  {
    title:"Tasks",
    icon:"assets/images/icon/master.png",
    child:[
      {
        title:"ADD NEW",
      },
      {
        title:"client",
        path:"admin/task/client",
      },
      {
        title:"job order",
        path:"admin/task/job-order",
      },
      {
        title:"employment Contract",
        path:"admin/task/employment-contract",
      },
      {
        title:"associate",
        path:"admin/task/associate",
      },

      {
        title:"configure",
      },
      {
        title:"claims",
        path:"admin/task/claims",
      },
      {
        title:"leave",
        path:"admin/task/leave",
      },
      {
        title:"timesheet",
        path:"admin/task/timesheet",
      },
      {
        title:"payslip",
        path:"admin/task/payslip",
      },
      {
        title:"roles & authorisation",
        path:"admin/task/roles-and-authorisation",
      }
    ]
  },
  
  {
    title:"blockchain console",
    path:"admin/blockchain-console",
    // iconclass:"fa fa-gear",
    icon:"assets/images/icon/blockchain.png"
  }
];