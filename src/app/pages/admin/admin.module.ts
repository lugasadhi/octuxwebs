import { NgModule } from '@angular/core';

// material
import {MatInputModule} from '@angular/material';

import { AdminComponent } from './admin.component';
import { AdminRoutingModule } from './admin-routing.module';

//bootstrap
import {
  NgbDropdownModule,
  
} from '@ng-bootstrap/ng-bootstrap';

//ngprime
import {TableModule} from 'primeng/table';

import { DashboardModule } from './view/home/dashboard.module';
import { BlockchainModule } from './view/blockchain/blockchain.module';

import { MenuComponent } from './shared/menu/menu.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HeaderComponent } from './shared/header/header.component';
import { BreadcrumbsComponent } from './shared/breadcrumbs/breadcrumbs.component';
import { CommonModule } from '@angular/common';



const ADMIN_COMPONENTS = [
  AdminComponent,
];

@NgModule({
  imports: [
    AdminRoutingModule,
    DashboardModule,
    BlockchainModule,
    CommonModule,

    // material
    MatInputModule,

    //bootsrap
    NgbDropdownModule,

    //ngprime
    TableModule,
  ],
  declarations: [
    ...ADMIN_COMPONENTS,
    MenuComponent,
    FooterComponent,
    HeaderComponent,
    BreadcrumbsComponent,
    // ClientComponent,
  ],
  exports: [],

})
export class AdminModule {
}
