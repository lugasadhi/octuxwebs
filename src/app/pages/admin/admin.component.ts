import { Component, OnInit, OnDestroy } from '@angular/core';
import * as $ from 'jquery';
import {AuthService} from '../../services/auth/auth.service';
import {GeneralService} from '../../services/general/general.service';


@Component({
  selector: 'ng-admin',
  template: `
    <div class="ng-admin" [ngClass]="{'admin-fixed': fixed, 'admin-static':!fixed}">
      <admin-header></admin-header> 
      <div class="app-mn-cntn">
        <app-menu class="addc-scroll"></app-menu>
        <div class="container-fluid app-cntent">
          <admin-breadcrumbs></admin-breadcrumbs>
          <div class="admin-content">
            <router-outlet></router-outlet>
          </div> 
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./general.scss']
})
export class AdminComponent  implements OnInit{

  fixed=true;

  constructor(
    private auth:AuthService,
    private general:GeneralService,
  ) {
   }

   ngOnInit() {
      $('body').addClass('admin-bg');
        
      // window.onbeforeunload = () => {
      //   if(!this.general.checkCookie("rememberme")){
      //     this.general.deleteCookie("token");
      //     this.general.deleteCookie("rememberme");
      //     this.auth.logout();
      //   }
      // }

    }

    ngOnDestroy(){
      $('body').removeClass('admin-bg');
    }
 
}
