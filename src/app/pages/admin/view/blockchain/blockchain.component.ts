import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import {GeneralService} from '../../../../services/general/general.service';
import {HistoryService} from '../../../../services/http/admin/history.service';

import * as $ from 'jquery';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {FormControl, Validators, FormGroup} from '@angular/forms';


@Component({
  selector: 'app-blockchain',
  templateUrl: './blockchain.component.html',
  styleUrls: ['./blockchain.component.scss']
})

export class BlockchainComponent implements OnInit,OnDestroy {
  searchForm;
  block_height = 0;
  last_transaction;
  historyData;
  maxPage=0;
  searhText='';
  page=1;
  pages = [];

  panelOpenState = false;
  displayedColumns: string[] = ['_id', 'timestamp', 'firstPartyName', 'firstPartyRole', 'action', 'secondPartyName', 'secondPartyRole'];
  dataSource;

  @ViewChild(MatSort) sort: MatSort;


  constructor(
    private _history:HistoryService,
    private _general:GeneralService
  ) { }

  ngOnInit() {
    $(".breadcrumb").hide();  

    this.last_transaction = new Date();

    this.searchForm = new FormGroup({
      search : new FormControl('', [])
    });

    this.getHistory();
  }

  ngOnDestroy(){
    $(".breadcrumb").show();    
  }

  search(){
    this.page = 1;
    this.searhText = this.searchForm.value.search;
    this.getHistory();
  }

  nextPage(){
    this.page ++;
    this.getHistory();
  }
  prevPage(){
    this.page --;
    this.getHistory();
  }

  getHistory(){
    this._general.setLoading(true);
    this._history.get(this.searhText,this.page ).subscribe(
      (resp)=>{
        this._general.setLoading(false);
        this.block_height = resp.blockHeight;
        this.historyData = this.changeData(resp.historyData);
        this.dataSource = new MatTableDataSource(this.historyData);
        this.dataSource.sort = this.sort;
        this.maxPage = resp.maxPageNum;
        this.last_transaction = resp.latestBlock.timestamp;
        this.pages = undefined;
        this.pages = []
        if(this.page > 5 && this.page < resp.maxPageNum-5){
          this.pages.push(this.page - 4);
          this.pages.push(this.page - 3);
          this.pages.push(this.page - 2);
          this.pages.push(this.page - 1);
          this.pages.push(this.page );
          this.pages.push(this.page + 1);
          this.pages.push(this.page + 2);
          this.pages.push(this.page + 3);
          this.pages.push(this.page + 4);
          this.pages.push(this.page + 5);
        }else if(this.page <= 5){
          this.pages.push(this.page );
          this.pages.push(this.page + 1);
          this.pages.push(this.page + 2);
          this.pages.push(this.page + 3);
          this.pages.push(this.page + 4);
          this.pages.push(this.page + 5);
          this.pages.push(this.page + 6);
          this.pages.push(this.page + 7);
          this.pages.push(this.page + 8);
          this.pages.push(this.page + 9);
        }else if(this.page >= resp.maxPageNum-5){
          this.pages.push(this.page - 9);
          this.pages.push(this.page - 8);
          this.pages.push(this.page - 7);
          this.pages.push(this.page - 6);
          this.pages.push(this.page - 5);
          this.pages.push(this.page - 4);
          this.pages.push(this.page - 3);
          this.pages.push(this.page - 2);
          this.pages.push(this.page - 1);
          this.pages.push(this.page );
        }
        
      },(err)=>{
        this._general.setLoading(false);
      }
    )
  }

  changeData(data){
    for (let i = 0; i < data.length; i++) {
      const element = data[i];
      data[i].firstPartyId6 =  data[i].firstPartyId.substr(data[i].firstPartyId.length - 6, data[i].firstPartyId.length);
      data[i].secondPartyId6 = data[i].secondPartyId.substr(data[i].secondPartyId.length - 6, data[i].secondPartyId.length);
      data[i].fkId = ((this.page*30)-30)+i+1;
    }
    return data;
  }

  clickPage(page){
    this.page = page;
    this.getHistory();
  }

  firstPage(){
    this.page = 1;
    this.getHistory();
  }

  lastPage(){
    this.page = this.maxPage;
    this.getHistory();
  }

  jumpNext(){
    for (let i = 0; i < this.pages.length; i++) {
      this.pages[i] = this.pages[i]+10;
    }
    console.log(this.pages[this.pages.length - 1]);
    
    if(this.pages[this.pages.length - 1] > this.maxPage){
      this.pages = undefined;
      this.pages = [];
      for (let i = 9; i >= 0; i--) {
        this.pages.push(this.maxPage-i);
      }
    }
  }
  
  jumpPrev(){
    for (let i = 0; i < this.pages.length; i++) {
      this.pages[i] = this.pages[i]-10;
    }
    if(this.pages[0] < 1){
      this.pages = undefined;
      this.pages = [];
      for (let i = 1; i <= 10; i++) {
        this.pages.push(i);
      }
    }
  }
  
  slpage = "1";
  slctPage(){
    this.page = parseInt(this.slpage);
    this.getHistory();
  }
}

