import { NgModule } from '@angular/core';
import { DashboardComponent } from './dashboard.component';
import {
  MatExpansionModule,
  MatFormFieldModule,
  MatInputModule,
} from '@angular/material';
import {NgbCollapseModule} from '@ng-bootstrap/ng-bootstrap'

@NgModule({
  imports: [
    // material
    MatExpansionModule,
    MatFormFieldModule,
    MatInputModule,

    // bootsrap
    NgbCollapseModule,
  ],
  declarations: [
    DashboardComponent,
  ],
})
export class DashboardModule { }
