import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

// material
import {
  MatInputModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatStepperModule,
  MatRadioModule,
  MatIconModule,
  MatSelectModule,
  MatCheckboxModule,
  MatTabsModule,
} from '@angular/material';

//bootstrap
import {
  NgbTooltipModule,
  NgbPopoverModule,
  NgbTabsetModule,
} from '@ng-bootstrap/ng-bootstrap';


import { TaskRoutingModule } from './task-routing.module';
import { TaskComponent } from './task.component';
import { ClientComponent } from './client/client.component';
import { AddClientComponent } from './client/add-client/add-client.component';
import { JobOrderComponent } from './job-order/job-order.component';
import { HirringProposalComponent } from './hirring-proposal/hirring-proposal.component';
import { EmploymentContractComponent } from './employment-contract/employment-contract.component';
import { AssociateComponent } from './associate/associate.component';
import { ClaimsComponent } from './claims/claims.component';
import { LeaveComponent } from './leave/leave.component';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { PayslipComponent } from './payslip/payslip.component';
import { RolesAndAuthorisationComponent } from './roles-and-authorisation/roles-and-authorisation.component';
import { EditClientComponent } from './client/edit-client/edit-client.component';
import { DraftComponent } from './employment-contract/draft/draft.component';
import { PendingSignatureComponent } from './employment-contract/pending-signature/pending-signature.component';
import { SignedComponent } from './employment-contract/signed/signed.component';
import { NewEmploymentContractComponent } from './employment-contract/new-employment-contract/new-employment-contract.component';


const ADMIN_COMPONENTS = [
];

@NgModule({
  imports: [
    CommonModule,
    TaskRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    // material
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatStepperModule,
    MatRadioModule,
    MatIconModule,
    MatSelectModule,
    MatCheckboxModule,
    MatTabsModule,
    

    // bootstrap
    NgbTooltipModule,
    NgbPopoverModule,
    NgbTabsetModule,


  ],
  declarations: [
    ...ADMIN_COMPONENTS,
    TaskComponent,
    ClientComponent,
    AddClientComponent,
    JobOrderComponent,
    HirringProposalComponent,
    EmploymentContractComponent,
    AssociateComponent,
    ClaimsComponent,
    LeaveComponent,
    TimesheetComponent,
    PayslipComponent,
    RolesAndAuthorisationComponent,
    EditClientComponent,
    DraftComponent,
    PendingSignatureComponent,
    SignedComponent,
    NewEmploymentContractComponent,
  ],
  exports: [],

})
export class TaskModule {
}
