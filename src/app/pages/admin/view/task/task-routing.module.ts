import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { TaskComponent } from './task.component';

import { ClientComponent } from './client/client.component';
import { AddClientComponent } from './client/add-client/add-client.component';
import { EditClientComponent } from './client/edit-client/edit-client.component';

import { JobOrderComponent } from './job-order/job-order.component';
import { HirringProposalComponent } from './hirring-proposal/hirring-proposal.component';

import { EmploymentContractComponent } from './employment-contract/employment-contract.component';
import { NewEmploymentContractComponent } from './employment-contract/new-employment-contract/new-employment-contract.component';


import { AssociateComponent } from './associate/associate.component';
import { ClaimsComponent } from './claims/claims.component';
import { LeaveComponent } from './leave/leave.component';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { PayslipComponent } from './payslip/payslip.component';
import { RolesAndAuthorisationComponent } from './roles-and-authorisation/roles-and-authorisation.component';


const routes: Routes = [
 
  {
    path: '',
    component: TaskComponent,
    children: [{
        path: 'client',
        component: ClientComponent,
      },
      {
        path: 'client/add-new-client',
        component: AddClientComponent,
      },
      {
        path: 'client/client-ID',
        component: EditClientComponent,
      },
      {
        path: 'job-order',
        component: JobOrderComponent,
      },
      {
        path: 'hirring-proposal',
        component: HirringProposalComponent,
      },
      {
        path: 'employment-contract',
        component: EmploymentContractComponent,
      },
      {
        path: 'employment-contract/create-new-employment-contract',
        component: NewEmploymentContractComponent,
      },
      {
        path: 'associate',
        component: AssociateComponent,
      },
      {
        path: 'claims',
        component: ClaimsComponent,
      },
      {
        path: 'leave',
        component: LeaveComponent,
      },
      {
        path: 'claims',
        component: ClaimsComponent,
      },
      {
        path: 'timesheet',
        component: TimesheetComponent,
      },
      {
        path: 'payslip',
        component: PayslipComponent,
      },
      {
        path: 'roles-and-authorisation',
        component: RolesAndAuthorisationComponent,
      },
      {
        path: '',
        redirectTo: 'client',
        pathMatch: 'full',
      }, 
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  // providers:[AuthGuard]
})
export class TaskRoutingModule {
}
