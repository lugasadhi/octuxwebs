import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {MatSort} from '@angular/material/sort';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {ClientServiceService} from '../client-service.service';
import {ClientService} from '../../../../../../services/http/admin/client.service';
import {ClientmanagerService} from '../../../../../../services/http/admin/clientmanager.service';
import {GeneralService} from '../../../../../../services/general/general.service';
import * as $ from 'jquery';


@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.scss']
})
export class EditClientComponent implements OnInit, OnDestroy {
  firstFormGroup: FormGroup;
  isEdit = false;
  paramId;


  constructor(
    private _formBuilder: FormBuilder,
    private _actRoute:ActivatedRoute,
    private _client:ClientService,
    private _clientManager:ClientmanagerService,
    private _general:GeneralService,
    private _generalClient:ClientServiceService,
    private router:Router
  ) { }

  displayedColumns = ['name', 'role', 'contact_no', 'email', 'action'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this._actRoute.queryParams
    .subscribe(params => {
      this.paramId = params.id; 
      if(this.paramId != undefined){
        this.setForm();
      }
    });

    this.days = this._generalClient.setDays(this.days);

    this.firstFormGroup = this._formBuilder.group({
      clientUEN: [{value:'',disabled:!this.isEdit}, Validators.required,],
      clientName: [{value:'',disabled:!this.isEdit}, Validators.required,],
      officeAddress1:[{value:'',disabled:!this.isEdit}, ],
      officeAddress2:[{value:'',disabled:!this.isEdit}, ],
      arrangementIsa:[{value:false,disabled:!this.isEdit}, Validators.required,],
      arrangementMsa:[{value:false,disabled:!this.isEdit}, Validators.required,],
    });
  }

  setForm(){
    this._general.setLoading(true);
    this._client.get(this.paramId,'').subscribe(
      (res:anyObject)=>{
        this._general.setLoading(false);
        this.firstFormGroup.setValue({
          clientUEN:res.wowId ,
          clientName:res.clientName ,
          officeAddress1:res.officeAddressLine1,
          officeAddress2:res.officeAddressLine2,
          arrangementIsa:this._generalClient.getArrangement(res,'isa'),
          arrangementMsa:this._generalClient.getArrangement(res,'msa'),
        });
        this.templates = this._generalClient.setParam(res.templates);
        this.templates = this._generalClient.setTemplate(this.templates);
        this.activeTemplate =  this.templates.length > 0? 1:0;
        this.templateChange(this.activeTemplate-1);
        this.getClientManager();
       
        let title = "Client ID: "+res.clientName +" (#"+ this.paramId.substring(this.paramId.length-6, this.paramId.length).toUpperCase()+")";
        $('.pageheader-title').hide();
        $( "<h2 class='pageheader-title ssdss'></h2>" ).insertBefore( ".pageheader-title" );
        $(".ssdss").css({
          "font-family": "Roboto",
          "font-size": "24px",
          "line-height": "1.5",
          "color": "#5cb8b2",
          "font-weight": "normal",
          "text-transform": "capitalize",
          "margin-bottom": "4px",
        })
        $(".ssdss, .breadcrumb-item:last-child span").html(title);
        
        console.log(res);
      },(err)=>{
        console.log(err); 
      }
    )
  }

  ngOnDestroy(): void {
    $('.pageheader-title').show();
    $('.ssdss').remove();
  }

  dbedit(id){
    this.edit();
    document.getElementById(id).focus();
  }

  edit(){
   this.isEdit = true;
   this.firstFormGroup.controls['clientUEN'].enable();
   this.firstFormGroup.controls['clientName'].enable();
   this.firstFormGroup.controls['officeAddress1'].enable();
   this.firstFormGroup.controls['officeAddress2'].enable();
   this.firstFormGroup.controls['arrangementIsa'].enable();
   this.firstFormGroup.controls['arrangementMsa'].enable();
  }

  setParameter(){
    let param = {
      clientId: this.paramId,
      wowId: this.firstFormGroup.value.clientUEN,
      clientName: this.firstFormGroup.value.clientName,
      officeAddressLine1: this.firstFormGroup.value.officeAddress1,
      officeAddressLine2: this.firstFormGroup.value.officeAddress2,
      arrangement: this._generalClient.setArrangement(this.firstFormGroup.value),
      templates:this._generalClient.setTemplateParam(this.templates),
    }
    return param;
  }

  save(){
    this.disableInput();  
    let param = this.setParameter();
    console.log(param);
    
    // this._general.setLoading(true);
    // this._client.put(param).subscribe(
    //   (res)=>{
    //     this._general.setLoading(false);
    //     if(this.newClienManager.length > 0){
    //       this.postClientManager(0, this.paramId);
    //     }
    //   },(err)=>{
    //     this._general.setLoading(false);
    //     console.log(err);
    //   }
    // )
  }

  postClientManager(index,clientID){
    this._general.setLoading(true);
    this.clientMng[index].clientId=clientID;
    this._clientManager.post(this.clientMng[index]).subscribe(
      (resp)=>{
        this.reCallPostClientManager(index, clientID);
      },(err)=>{
        console.log(err);
        // this.genPopup.value = true;
        // this.genPopup.msg = err.error.message;
        this.reCallPostClientManager(index, clientID);
      }
    )
  }

  reCallPostClientManager(index, clientID){
    if(index < this.clientMng.length -1 ){
      index++;
      this.postClientManager(index,clientID);
    }else{
      // this.draftPop = true;
      this._general.setLoading(false);
      this.router.navigate(['/admin/task/client'] );
    }
  }

  disableInput(){
    this.isEdit = false;
    this.firstFormGroup.controls['clientUEN'].disable();
    this.firstFormGroup.controls['clientName'].disable();
    this.firstFormGroup.controls['officeAddress1'].disable();
    this.firstFormGroup.controls['officeAddress2'].disable();
    this.firstFormGroup.controls['arrangementIsa'].disable();
    this.firstFormGroup.controls['arrangementMsa'].disable();
  }

  // =======================================================
  //                        users
  // =======================================================
  tblEdit=false;
  deletePopup = false;
  selectedRowIndex;

  clnt_nm;clnt_rl;clnt_c_no;clnt_email;

  chck(userdata){
    if(!this.isNewUserCreate){
      if(this.selectedRowIndex != userdata.ids){
        this.tblEdit = false;
      }
      this.selectedRowIndex = userdata.ids;
    }
  }

  isNewUserCreate= false;
  
  newUser(){
    if(!this.isNewUserCreate){
      this.isNewUserCreate = true;
      this.tblEdit = true;

      if(this.clientMng == undefined){
        this.clientMng = [];
      }
      
      this.clientMng.unshift({
        ids:(Math.floor(Math.random() * Math.floor(1000))).toString(),
        clientName: '',
        clientManagerName:'',
        jobTitle: '',
        phoneNumber: '',
        email: '',
        draftId:''
      });
      
      this.selectedRowIndex = this.clientMng[0].ids;
      this.clnt_nm='';
      this.clnt_rl= '';
      this.clnt_c_no ='';
      this.clnt_email ='';

      this.dataSource = new MatTableDataSource(this.clientMng);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    }
  }

  editRow(data){
    this.tblEdit = true;
    this.clnt_nm = data.clientManagerName;
    this.clnt_rl = data.jobTitle;
    this.clnt_c_no = data.phoneNumber;
    this.clnt_email = data.email;
  }

  deleteIds;
  deleteRow(data){
    this.deleteIds = data.ids;
    this.deletePopup = true;
  }

  onKeydown(event, data){
    if (event.key === "Enter") {
      this.doneRow(data);
    }
  }

  newClienManager=[];
  doneRow(data){
    if(this.dataRowIsValid()){
      let param = {
        clientName : this.firstFormGroup.value.clientName,
        clientManagerName: this.clnt_nm ,
        jobTitle:  this.clnt_rl ,
        phoneNumber:  this.clnt_c_no,
        email: this.clnt_email,
        clientManagerId:data.clientManagerId,
        ids:data.ids
      }
      if(this.isNewUserCreate){
        this.isNewUserCreate = false;
        this.tblEdit = false;
        this.newClienManager=[...this.newClienManager, param];
        this.clientMng[0] = param;
      }else{
        this.tblEdit = false;
        for (let i = 0; i < this.clientMng.length; i++) {
          if(this.clientMng[i].ids == data.ids){
        
            this._general.setLoading(true);
            this._clientManager.put(param).subscribe(
              (res)=>{
                  this._general.setLoading(true);
                  this.clientMng[i] = param;
              },
              (err)=>{
                this._general.setLoading(true);
                console.log(err);
              }
            )

            break;
          }
        }
      }
      this.dataSource = new MatTableDataSource(this.clientMng);
    }
  }

  dataRowIsValid(){
    if(this.clnt_nm  != '' && 
      this.clnt_rl  != '' && 
      this.clnt_c_no != '' && 
      this.clnt_email != '' ) {
        if(isNaN(this.clnt_c_no )){return false}
        return true;
      }else{
        return false;
      }
  }
  
  deleteAccountTable(){
    let isNew= false;
    for (let i = 0; i < this.clientMng.length; i++) {
      if(this.clientMng[i].ids == this.deleteIds){
        if(this.clientMng[i].draftId == ''){
          isNew = true;
          this.clientMng.splice(i,1);
        }
        else{
          this._general.setLoading(true);
          this._clientManager.delete(this.clientMng[i].clientManagerId).subscribe(
            (res)=>{if(res){
              this._general.setLoading(false);
              this.clientMng.splice(i,1);
              this.dataSource = new MatTableDataSource(this.clientMng);
            }},
            (err)=>{
              this._general.setLoading(false);
              console.log(err);
            }
          )
        }
        break;
      }
    }

    for (let i = 0; i < this.newClienManager.length; i++) {
      if(this.newClienManager[i].ids == this.deleteIds){
        this.newClienManager.splice(i,1);
      }
    }
    
    this.dataSource = new MatTableDataSource(this.clientMng);
    this.deletePopup = false;
  }


  clientMng;
  clientMngOri;
  getClientManager(){
    this._clientManager.get('',this.paramId).subscribe(
      (resp)=>{
        this.clientMng = resp;
        this.clientMngOri = resp;
        for (let i = 0; i < this.clientMng.length; i++) {
          //  this.clientMng[i].draftId = resp[i]._id;
           this.clientMng[i].ids=(Math.floor(Math.random() * Math.floor(1000))).toString();
        }
        
        this.dataSource = new MatTableDataSource(this.clientMng);
      },(err)=>{
        console.log(err);
      }
    )
  }

  

  // ================================================
  //                     templates
  //  ===============================================

  days;
  deleteTemplatePopup=false;
  dmy = this._generalClient.probationPeriodeTime;
  dmys = this._generalClient.fixedOtPer;
  timepicker= this._generalClient.time;
  ampm=this._generalClient.ampm;
  dayType=this._generalClient.otTimeType;
  hour=this._generalClient.hour;

  mon = [];
  tue = [];
  wed = [];
  thu = [];
  fri = [];
  sat = [];
  sun = [];
  
  startTimeCnf(ds){
    if(this.templates[this.activeTemplate - 1].changeTimeSetting.start.ampm == "pm"){
      this.templates[this.activeTemplate - 1].workingHours.startTime = parseInt(this.templates[this.activeTemplate - 1].changeTimeSetting.start.time) + 1200;
    }else{
      this.templates[this.activeTemplate - 1].changeTimeSetting.start.ampm = "am";
      this.templates[this.activeTemplate - 1].workingHours.startTime = parseInt(this.templates[this.activeTemplate - 1].changeTimeSetting.start.time) ;
    }
    this.templates[this.activeTemplate - 1].workingHours.startTime = this.templates[this.activeTemplate - 1].workingHours.startTime.toString();
    if(this.templates[this.activeTemplate - 1].workingHours.startTime.length == 3){
      this.templates[this.activeTemplate - 1].workingHours.startTime =  "0"+this.templates[this.activeTemplate - 1].workingHours.startTime;
    }
  }

  endTimeCnf(ds){
    if(this.templates[this.activeTemplate - 1].changeTimeSetting.end.ampm == "pm"){
      this.templates[this.activeTemplate - 1].workingHours.endTime = parseInt(this.templates[this.activeTemplate - 1].changeTimeSetting.end.time) + 1200;
    }else{
      this.templates[this.activeTemplate - 1].changeTimeSetting.end.ampm = "am";
      this.templates[this.activeTemplate - 1].workingHours.endTime = parseInt(this.templates[this.activeTemplate - 1].changeTimeSetting.end.time) ;
    }

    this.templates[this.activeTemplate - 1].workingHours.endTime = this.templates[this.activeTemplate - 1].workingHours.endTime.toString();
    if(this.templates[this.activeTemplate - 1].workingHours.endTime.length == 3){
      this.templates[this.activeTemplate - 1].workingHours.endTime =  "0"+this.templates[this.activeTemplate - 1].workingHours.endTime;
    }
  }

  setWorkingDaysPerWeek(data,i){
    this.templates[this.activeTemplate - 1].workingDaysPerWeek[i]=data;
  }

  templates=[];
  templateWorkingWeek=[];
  activeTemplate=0;
  ttlDisabled = true;

  addTemplate(){
    this.ttlDisabled = true;
    let param = this._generalClient.addTemplate(this.templates);
    this.templates.push(param);
    this.activeTemplate = this.templates.length;
  }

  templateChange(idx){
    this.activeTemplate = idx+1;
    this.ttlDisabled = true;
    this.mon = this.templates[this.activeTemplate-1].workingDaysPerWeek[0];
    this.tue = this.templates[this.activeTemplate-1].workingDaysPerWeek[1];
    this.wed = this.templates[this.activeTemplate-1].workingDaysPerWeek[2];
    this.thu = this.templates[this.activeTemplate-1].workingDaysPerWeek[3];
    this.fri = this.templates[this.activeTemplate-1].workingDaysPerWeek[4];
    this.sat = this.templates[this.activeTemplate-1].workingDaysPerWeek[5];
    this.sun = this.templates[this.activeTemplate-1].workingDaysPerWeek[6];
  }

  deleteTemplate(){
    this.templates.splice(this.activeTemplate-1,1);
    this.activeTemplate = 0;
    this.deleteTemplatePopup = false;
    this.ttlDisabled = true;
  }

}

interface anyObject {
  [key: string]: any
}
