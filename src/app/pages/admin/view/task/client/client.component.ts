import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import {Router} from '@angular/router';
import {GeneralService} from '../../../../../services/general/general.service';
import {ClientService} from '../../../../../services/http/admin/client.service';



@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.scss'],
  styles: [`
    ngb-popover-window{
      background:#000;
    }
  `]
})
export class ClientComponent implements OnInit {
  allClient;
  sclient;
  dclient;

  constructor(
    private route:Router,
    private general:GeneralService,
    private client:ClientService,
  ) { }

  displayedColumns = ['clientId', 'clientName', 'status', 'created_by', 'date_modifield','action'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.getDraftClient('',0);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  
  goto(data){
    if(data.status == 'draft'){
      this.route.navigate(['/admin/task/client/add-new-client'],{queryParams: {id:data.id}});
    }else if(data.status == 'complete'){
      this.route.navigate(['/admin/task/client/client-ID'],{queryParams: {id:data.clientId}});
    }
  }


  getClient(id,page){
    this.general.setLoading(true);
    this.client.get(id,page).subscribe(
      (resp)=>{
        this.general.setLoading(false);
        this.sclient = resp;
        for (let i = 0; i < this.sclient.length; i++) {
          this.sclient[i].types = 'client';
        }
        this.margeClient();
      },(err)=>{
        this.general.setLoading(false);
      }
    );
  }
    
  getDraftClient(id,page){
    this.dclient = undefined;
    this.dclient =[];

    this.general.setLoading(true);
    this.client.getDraft(id,page).subscribe(
      (resp:Array<object>)=>{
        this.general.setLoading(false);
        this.dclient = resp;
        for (let i = 0; i < this.dclient.length; i++) {
          this.dclient[i].types = 'draft';
        }
        if(resp.length < 50){
          this.getClient(id,page);
        }else{
          this.allClient = resp;
        }
      },(err)=>{
        this.general.setLoading(false);
      }
    );
  }

  margeClient(){
    this.allClient = undefined;
    this.allClient = [];
    for (let index = 0; index < this.dclient.length; index++) {
      let param = {
        clientId:"",
        clientId6:"",
        clientName: this.dclient[index].clientName,
        status:"draft",
        created_by: this.dclient[index].createdByName,
        id: this.dclient[index]._id,
        date_modifield:this.dclient[index].lastModifiedTime,
      }
      this.allClient.push(param);
    }

    for (let index = 0; index < this.sclient.length; index++) {
      let param = {
        clientId:this.sclient[index].Record.clientId,
        clientId6:this.sclient[index].Record.clientId.substring(this.sclient[index].Record.clientId.length-6, this.sclient[index].Record.clientId.length),
        clientName: this.sclient[index].Record.clientName,
        status:"complete",
        created_by: this.sclient[index].Record.createdByName,
        id:"",
        date_modifield:"",
      }
      this.allClient.push(param);
    }

    this.dataSource = new MatTableDataSource<clientStatus>(this.allClient);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  
  last6(data){
    return data.substring(data.length-6, data.length);
  }
  deleteConfirm ={
    show:false,
    data:{}
  }
  deleteClient(data){
    if(data.status == 'draft'){
      this.client.deleteDraft(data.id).subscribe((resp)=>{
        this.getDraftClient('',0);
        this.deleteConfirm.show = false;
      });
    }else{
      this.client.delete(data.clientId).subscribe((resp)=>{
        this.getDraftClient('',0);
        this.deleteConfirm.show = false;
      });
    }
  }
 
}



interface clientStatus {
  clientId: string;
  clientId6: string;
  clientName: string;
  status: string;
  created_by: string;
  date_modifield: string,
}

interface userDt{
  adminId: String,
  email: String,
  name: String,
  phoneNumber: String,
  role: String,
}