import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolesAndAuthorisationComponent } from './roles-and-authorisation.component';

describe('RolesAndAuthorisationComponent', () => {
  let component: RolesAndAuthorisationComponent;
  let fixture: ComponentFixture<RolesAndAuthorisationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolesAndAuthorisationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolesAndAuthorisationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
