import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HirringProposalComponent } from './hirring-proposal.component';

describe('HirringProposalComponent', () => {
  let component: HirringProposalComponent;
  let fixture: ComponentFixture<HirringProposalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HirringProposalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HirringProposalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
