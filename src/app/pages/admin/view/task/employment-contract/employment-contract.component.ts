import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';


@Component({
  selector: 'app-employment-contract',
  templateUrl: './employment-contract.component.html',
  styleUrls: ['./employment-contract.component.scss']
})
export class EmploymentContractComponent implements OnInit {
  isCreateContract = false;
  constructor() { }

  ngOnInit() {
  }

  tabChange(data){
    let rtn = false;
    if(data.toElement.id=='draft'){rtn = true;}
    else if(this.isCreateContract && data.toElement.id==''){rtn = true;}
    else{rtn = false;}
    this.isCreateContract = rtn;
  }

  
}
