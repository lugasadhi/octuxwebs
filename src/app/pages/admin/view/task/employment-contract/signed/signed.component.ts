import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';


@Component({
  selector: 'emp-cont-signed',
  templateUrl: './signed.component.html',
  styleUrls: ['./signed.component.scss']
})
export class SignedComponent implements OnInit {

 
  displayedColumns = ['ec_id', 'client_name', 'job_order_name', 'associate_id', 'associate_name', 'attachment'];
  dataSource;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  data:Array<dataInf> = [
    {
      'ec_id':"1242", 
      'client_name':"Contract Receptionist", 
      'job_order_name':"", 
      'associate_id':"", 
      'associate_name':'',
      'attachment':"",
    },
    {
      'ec_id':"1242", 
      'client_name':"", 
      'job_order_name':"", 
      'associate_id':"", 
      'associate_name':'',
      'attachment':"",
    }
  ]

  constructor() { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource<dataInf>(this.data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

}


interface dataInf{
  'ec_id':string, 
  'client_name':string, 
  'job_order_name':string, 
  'associate_id':string,
  'associate_name':string, 
  'attachment':string,
}