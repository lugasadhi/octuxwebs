import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingSignatureComponent } from './pending-signature.component';

describe('PendingSignatureComponent', () => {
  let component: PendingSignatureComponent;
  let fixture: ComponentFixture<PendingSignatureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingSignatureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingSignatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
