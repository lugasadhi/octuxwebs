import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccosiatesComponent } from './accosiates.component';

describe('AccosiatesComponent', () => {
  let component: AccosiatesComponent;
  let fixture: ComponentFixture<AccosiatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccosiatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccosiatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
