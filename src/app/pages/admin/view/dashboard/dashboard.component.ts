import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'ng-dashboard',
  template: `
      <router-outlet></router-outlet>
  `,
  // styleUrls: ['./general.scss']
})
export class DashboardComponent  implements OnInit{

  constructor() {
   }

   ngOnInit() {
    }

    ngOnDestroy(){
    }
 
}
