import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { DashboardComponent } from './dashboard.component';
import { AccosiatesComponent } from './associates/accosiates.component';
import { ClaimsComponent } from './claims/claims.component';
import { LeaveComponent } from './leave/leave.component';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { PayslipComponent } from './payslip/payslip.component';


const routes: Routes = [
 
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'accosiates',
        component: AccosiatesComponent,
      },
      {
        path: 'claims',
        component: ClaimsComponent,
      },
      {
        path: 'leave',
        component: LeaveComponent,
      },
      {
        path: 'timesheet',
        component: TimesheetComponent,
      },
      {
        path: 'payslip',
        component: PayslipComponent,
      },
      {
        path: '',
        redirectTo: 'accosiates',
        pathMatch: 'full',
      }, 
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  // providers:[AuthGuard]
})
export class DashboardRoutingModule {
}
