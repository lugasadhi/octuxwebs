import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSort} from '@angular/material/sort';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
import {ClaimService} from '../../../../../services/http/admin/claim.service';


@Component({
  selector: 'app-claims',
  templateUrl: './claims.component.html',
  styleUrls: ['./claims.component.scss']
})
export class ClaimsComponent implements OnInit {

  displayedColumns = ['associateId', 'claimName', 'noReceiptRemarks', 'currencyAmount', 'status','attachment', 'submissionDate','receiptDate'];
  dataSource;
  
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  claimDtl;
  popupDetail = false;
  isFilter = false;
  isEditAmount = false;
  isRejectClicked = false;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  exportAsConfig: ExportAsConfig = {
    type: 'xls', // the type you want to download
    elementId: 'exportTable', // the id of html/table element
  }

  constructor(
    private exportAsService: ExportAsService,
    private _claim: ClaimService,
  ) { }

  data = [
    {
      'associateId':"892374892374982",
      'name':"jane lim", 
      'nric':"S2919100E", 
      'ammount':23.92, 
      'status':"approved",
      'attachment':[], 
      'date_approved':"2019-07-16T07:43:38.252Z",
      'date_submited':"2019-07-16T07:43:38.252Z"
    },
    {
      'associateId':"0348548453845",
      'name':"jane lim", 
      'nric':"S2919100E", 
      'ammount':23.92, 
      'status':"rejected",
      'attachment':['https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf'], 
      'date_approved':"",
      'date_submited':"2019-07-16T07:43:38.252Z"
    },
    {
      'associateId':"0981230901238927",
      'name':"jane lim", 
      'nric':"S2919100E", 
      'ammount':23.92, 
      'status':"pending approval",
      'attachment':['https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf', 'https://collegereadiness.collegeboard.org/pdf/sat-practice-test-8.pdf'], 
      'date_approved':"",
      'date_submited':"2019-07-16T07:43:38.252Z"
    },
    {
      'associateId':"0981230901238927",
      'name':"jane lim", 
      'nric':"S2919100E", 
      'ammount':23.92, 
      'status':"processed",
      'attachment':['https://collegereadiness.collegeboard.org/pdf/sat-practice-test-8.pdf', 'https://vadimdez.github.io/ng2-pdf-viewer/assets/pdf-test.pdf'], 
      'date_approved':"2019-07-16T07:43:38.252Z",
      'date_submited':"2019-07-16T07:43:38.252Z"
    }
  ]
  

  ngOnInit() {
    for (let index = 0; index < this.data.length; index++) {
      let s = {associate_id: this.data[index].associateId.substring(this.data[index].associateId.length-6,this.data[index].associateId.length)}
      this.data[index] = { ...this.data[index], ...s} 
    }
    this.dataSource = new MatTableDataSource(this.data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.http_getClaimList(0);
  }

  http_getClaimList(page){
    let param ={
      pageNumber:page
    };
    this._claim.get(param).subscribe(
      (resp:Array<anyObject>)=>{
        console.log(resp);
        let tbl = [];
        for (let i = 0; i < resp.length; i++) {
          resp[i].Record.attachment = [];
          // resp[i].Record.submissionDate = new Date(resp[i].Record.submissionDate);
          tbl = [...tbl, resp[i].Record]
        }
        this.data = tbl;
        this.dataSource = new MatTableDataSource(this.data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }
    )
  }




  isDataPrevAct = true;
  isDataNextAct = true;
  showClaimDetail(data){
    
    this.attchIdx = 0;
    this.isRejectClicked = false;
    this.isEditAmount = false;
    this.popupDetail = true;
    this.claimDtl = data;
    let id = this.checkArrayNumber();
    this.checkNextPrevActive(id);
  }

  prevData(){
    let id = this.checkArrayNumber();
    this.checkNextPrevActive(id-1);
    let prevId = this.data[id-1];
    this.claimDtl = prevId;
  }
  
  nextData(){
    let id = this.checkArrayNumber();
    this.checkNextPrevActive(id+1);
    let nextId = this.data[id+1];
    this.claimDtl = nextId;
  }

  checkNextPrevActive(idx){
    this.isDataPrevAct = idx==0?false:true;
    this.isDataNextAct = idx==this.data.length-1?false:true;
  }

  checkArrayNumber(){
    for (let i = 0; i < this.data.length; i++) {
      if(this.data[i].associateId == this.claimDtl.id){
        return i;
      }
    }
  }

  filterData(){
    this.isFilter = !this.isFilter;
  }

  isShowFilter={
    status:false,
    show:-1,
    isOutside: false
  }
  showFilter(data){
    this.isOutsideAct = 0;
    this.isShowFilter.status = this.isShowFilter.show==data? !this.isShowFilter.status : true;
    this.isShowFilter.show = data;
    this.isShowFilter.isOutside = false;
  }

  isOutsideAct:number;
  clickOutsideFilter(index){
    setTimeout(()=>{
      if(this.isOutsideAct == 0){
        this.isShowFilter.status = (this.isShowFilter.status &&  this.isShowFilter.isOutside )?false:true;
        this.isShowFilter.isOutside = true;
      }else if(this.isOutsideAct == 2){
        this.isOutsideAct = 0;
      }
    }, 100);
  }

  isOutsides(data){
    this.isOutsideAct = data;
  }

  export() {
    this.exportAsService.save(this.exportAsConfig, 'Claim List').subscribe((ss) => {
      console.log(ss);
    });
  }

  pdfshow = true;
  attchIdx = 0;
  viewMoreAttch(lng){
    this.attchIdx = this.attchIdx <lng-1 ? this.attchIdx +1 :0;
  }

  isPdfLoad = false;
  pdfLoad(render){
    this.isPdfLoad = render.total == undefined?true:false;
  }

  popupDetPdf={
    show:false,
    source:''
  }

  showPopUpPDF(source){
    this.popupDetPdf.show = true;
    this.popupDetPdf.source = source;
  }

  rejectClaim(){
    if(this.isRejectClicked){
      
    }
    this.isRejectClicked = true;
  }

}


interface anyObject {
  [key: string]: any
}
