import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupDetailPdfComponent } from './popup-detail-pdf.component';

describe('PopupDetailPdfComponent', () => {
  let component: PopupDetailPdfComponent;
  let fixture: ComponentFixture<PopupDetailPdfComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupDetailPdfComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupDetailPdfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
