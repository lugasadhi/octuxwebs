import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {FormControl} from '@angular/forms';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';

import * as _moment from 'moment';
const moment =  _moment;

export const MY_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'DD/MM/YYYY',
    monthYearA11yLabel: 'MMMM YYYY',
  },
};


@Component({
  selector: 'filtertable',
  templateUrl: './filtertable.component.html',
  styleUrls: ['./filtertable.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MY_FORMATS},
  ],
})
export class FiltertableComponent implements OnInit {

  @Input('type') type: string;
  @Input('inputType') inputType: string;

  @Output() isOutside = new EventEmitter<number>();


  dataCheck=[];
  claims_type;
  date = new FormControl(moment());

  constructor() { }

  ngOnInit() {
    this.dummy();
  }

  dummy(){
    this.dataCheck = undefined;
    this.dataCheck = [];

    if(this.type == 'associate_id'){
      this.claims_type = 'Associate ID';
      this.dataCheck=["#213123",'#11111','#5234234'];
      this.dataCheck.filter(s => s.includes('#111'))
    }
    else if(this.type == 'name'){
      this.claims_type = 'Name';
      this.dataCheck=["Jane Lim",'Janes'];
    }
    else if(this.type == 'nric'){
      this.claims_type = 'NRIC';
      this.dataCheck=["S2919100E",'S291ED9100E'];
    }
    else if(this.type == 'ammount'){
      this.claims_type = 'Amount';
      this.dataCheck=["$0 - $20",'$20.01 - $50','$50.01 - $100', 'above $161'];
    }
    else if(this.type == 'status'){
      this.claims_type = 'Status';
      this.dataCheck=["Approved",'Rejected','Pending Approval','Processed'];
      // for (let i = 0; i < this.dataCheck.length; i++) {
      // }
    }
    else if(this.type == 'attachment'){
      this.claims_type = 'Attachment';
      this.dataCheck=['Attachment','Multiple Attachments','No Attachment'];
    }
    else if(this.type == 'date_submitted'){
      this.claims_type = 'Date Submitted';
      this.dataCheck=['2010','2011','2012'];
    }
    else if(this.type == 'date_approved'){
      this.claims_type = 'Date Approved';
      this.dataCheck=['2010','2011','2012'];
    }
  }

  datepickerAct(data){
    this.isOutside.emit(data);
  }

}
