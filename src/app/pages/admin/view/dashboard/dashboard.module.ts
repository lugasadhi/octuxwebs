import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ClickOutsideModule } from 'ng-click-outside';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import { ExportAsModule } from 'ngx-export-as';

// material
import {
  MatInputModule,
  MatTableModule,
  MatSortModule,
  MatPaginatorModule,
  MatStepperModule,
  MatRadioModule,
  MatIconModule,
  MatSelectModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatProgressSpinnerModule, 
} from '@angular/material';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { AccosiatesComponent } from './associates/accosiates.component';
import { ClaimsComponent } from './claims/claims.component';
import { LeaveComponent } from './leave/leave.component';
import { TimesheetComponent } from './timesheet/timesheet.component';
import { PayslipComponent } from './payslip/payslip.component';
import { FiltertableComponent } from './claims/filtertable/filtertable.component';
import { PopupDetailPdfComponent } from './claims/popup-detail-pdf/popup-detail-pdf.component';


const ADMIN_COMPONENTS = [
];

@NgModule({
  imports: [
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,

    //other
    ClickOutsideModule,
    ExportAsModule,
    PdfViewerModule,

    // material
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatStepperModule,
    MatRadioModule,
    MatIconModule,
    MatSelectModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatNativeDateModule,

  ],
  declarations: [
    ...ADMIN_COMPONENTS,
    DashboardComponent,
    AccosiatesComponent,
    ClaimsComponent,
    LeaveComponent,
    TimesheetComponent,
    PayslipComponent,
    FiltertableComponent,
    PopupDetailPdfComponent,
  ],
  exports: [],

})
export class DashboardModule {
}
