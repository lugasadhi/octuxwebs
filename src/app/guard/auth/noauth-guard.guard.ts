import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import {GeneralService} from '../../services/general/general.service';

@Injectable({
  providedIn: 'root'
})
export class NoauthGuard implements  CanActivate {
  constructor(
    private general:GeneralService,
    private route:Router,
  ) {}
checkAuthLogin
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let token = this.general.getCookies("token");
    if(token == undefined || token == ''){
      return true;
    }else{
      this.route.navigate(['/admin/']);
      return false;
    }
  }


}