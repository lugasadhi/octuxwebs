import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import {GeneralService} from '../../services/general/general.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements  CanActivate {
  constructor(
    private _general:GeneralService,
    private route:Router,
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    var token = this._general.getCookies("token");
    if(token == undefined || token == ''){
      this.route.navigate(['auth/login']);
      return false;
    }else{
      return true;
    }
  }


}