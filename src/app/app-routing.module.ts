import { NgModule } from '@angular/core';
import { ExtraOptions, Routes, RouterModule } from '@angular/router';
import { P404Component } from './pages/pagenotfound/p404.component';
import { AuthGuard } from './guard/auth/auth-guard.guard';
import { NoauthGuard } from './guard/auth/noauth-guard.guard';

const routes: Routes = [
  { 
    path: 'admin', 
    loadChildren: './pages/admin/admin.module#AdminModule', 
    // canActivate:[AuthGuard],
  },
  { 
    path: 'view', 
    loadChildren: './pages/view/view.module#ViewModule',
  },
  { 
    path: 'auth', 
    loadChildren: './pages/auth/auth.module#AuthModule',
    // canActivate:[NoauthGuard],
  },
  { path: '',
   redirectTo: 'auth', 
   pathMatch: 'full',  
  },
  { path: '**', component: P404Component },
];

const config: ExtraOptions = {
  useHash: true,
};

@NgModule({
  imports: [RouterModule.forRoot(routes, config)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
